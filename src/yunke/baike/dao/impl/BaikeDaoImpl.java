package yunke.baike.dao.impl;

import yunke.baike.dao.BaikeDao;
import yunke.baike.dao.BaseDao;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 2017/1/7.
 */
public class BaikeDaoImpl extends BaseDao implements BaikeDao{

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public int addBaikeInfo(String name, String content)
    {
        int count = 0;
        String sql = "insert into company_yunke_baike values(?,?,?,?)";
        count = super.executeUpdate(sql, new Object[] {null, name, content, sdf.format(new Date())});
        return count;
    }

}
