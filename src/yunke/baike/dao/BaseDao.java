package yunke.baike.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDao {

//	private String URL = "jdbc:mysql://localhost:3306/baike?useUnicode=true&characterEncoding=utf-8";//&useSSL=false";
//	private String DRIVER = "com.mysql.jdbc.Driver";
//	private String USER = "root";
//	private String PWD = "root";

	private String URL = "jdbc:mysql://rds0710650me01y6d3ogo.mysql.rds.aliyuncs.com:3306/dataprocess";
	private String DRIVER = "com.mysql.jdbc.Driver";
	private String USER = "yunkedata";
	private String PWD = "XingDongJia@2016";

	public Connection getConn() {
		Connection conn = null;
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PWD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public void closeAll(ResultSet rs, Statement stmt, Connection conn) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet executeQuery(String sql, Object... obj) {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Connection conn = getConn();
		try {
			pstmt = conn.prepareStatement(sql);
			System.out.println(conn);
			if (obj != null) {
				for (int i = 0; i < obj.length; i++) {
					pstmt.setObject(i + 1, obj[i]);
				}
			}
			rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	public int executeUpdate(String sql, Object... obj) {
		int result = 0;
		PreparedStatement pstmt = null;
		Connection conn = getConn();
		try {
			pstmt = conn.prepareStatement(sql);
			if (obj != null) {
				for (int i = 0; i < obj.length; i++) {
					pstmt.setObject(i + 1, obj[i]);
				}
			}
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll(null, pstmt, conn);
		}
		return result;
	}

}

