package yunke.baike.process;

import org.apache.commons.lang.StringUtils;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;


/**
 * Created by admin on 2017/2/17.
 */
public class YangLao implements PageProcessor{

    private Site site = Site.me().setRetryTimes(3).setRetrySleepTime(3000).setTimeOut(60000)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

    int count = 1;
    String url = "http://www.yanglaocn.com/yanglaoyuan/yly/?&RgSelect=01001&BNSelect=1&BTSelect=1&PRSelect=1&NaSelect=1&skey=&page=1";
    @Override
    public void process(Page page) {

        if (page.getUrl().regex("page=[0-9]+").match()){
            count++;
            page.addTargetRequests(page.getHtml().xpath("//div[@class='jiadiantucontext_ul']/a").links().all());
            if(count < 88){
                page.addTargetRequest(url.replaceAll("page=1","page=" + count));
            }

        }else{
            page.putField("name", page.getHtml().getDocument().select("div[class=leftcontexttitle]").get(0).select("label").text());
            page.putField("company", page.getHtml().getDocument().select("h3").text());
            page.putField("lxr", StringUtils.substringBetween(page.getHtml().toString(),"联系人：</span>","<").trim());
            page.putField("phone", StringUtils.substringBetween(page.getHtml().toString(),"固定电话：</span>","<").trim());
            page.putField("moblie", StringUtils.substringBetween(page.getHtml().toString(),"手机号码：</span>","<").trim());
            page.putField("address", StringUtils.substringBetween(page.getHtml().toString(),"联系地址：</span>","<").trim());
        }



    }

    @Override
    public Site getSite() {
        return site;
    }


    public static void main(String[] args) {
        Spider.create(new YangLao())
                .addUrl("http://www.yanglaocn.com/yanglaoyuan/yly/?&RgSelect=01001&BNSelect=1&BTSelect=1&PRSelect=1&NaSelect=1&skey=&page=1")
                .addPipeline(new JsonFilePipeline("d:/Data/Yanglao/"))
                .addPipeline(new ConsolePipeline())
                .thread(5)
                .start();
    }


}
