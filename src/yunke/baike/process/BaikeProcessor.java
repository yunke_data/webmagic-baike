package yunke.baike.process;

import us.codecraft.webmagic.*;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.QueueScheduler;
import us.codecraft.webmagic.scheduler.component.BloomFilterDuplicateRemover;
import yunke.baike.dao.BaikeDao;
import yunke.baike.dao.impl.BaikeDaoImpl;

import java.io.*;
import java.util.LinkedList;

/**
 * Created by admin on 2017/1/7.
 */
public class BaikeProcessor implements PageProcessor{


    private Site site = Site.me().setRetryTimes(3).setRetrySleepTime(3000).setTimeOut(60000)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

    LinkedList<String> list = getCompany("d:\\crawler\\yunke_company_title_20160919.txt");

    @Override
    public void process(Page page) {


        page.putField("name", page.getHtml().getDocument().title().replaceAll("_百度百科",""));
        if(page.getResultItems().get("name").equals("百度百科——全球最大中文百科全书") ||
                page.getResultItems().get("name").equals("北京联云天下科技有限公司")){
            System.out.println(page.getUrl());
            page.setSkip(true);
        }
        page.putField("content",page.getHtml().getDocument().text());
        if(page.getTargetRequests().size() == 0){
            if(list.size() > 50){
                while(true){
                    if(page.getTargetRequests().size() == 50){
                        break;
                    }
                    synchronized (list){
                        page.addTargetRequest(list.removeFirst());
                    }
                }
            }else{
                while (true){
                    if(list.size() == 0){
                        break;
                    }
                    synchronized (list){
                        page.addTargetRequest(list.removeFirst());
                    }
                }
            }
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        BaikeDao baikeDao = new BaikeDaoImpl();
        Spider.create(new BaikeProcessor())
                .addUrl("http://baike.baidu.com/item/北京联云天下科技有限公司")
                .addPipeline(new Pipeline() {
                    @Override
                    public void process(ResultItems items, Task task) {
                        baikeDao.addBaikeInfo(items.get("name").toString(), items.get("content").toString());
                        System.out.println("Company: " + items.get("name").toString());
                    }
                })
                .setScheduler((new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(10000000))))
                .thread(20)
                .run();
    }



    public LinkedList<String> getCompany(String path){
        LinkedList<String> list = new LinkedList<>();
        File file = new File(path);//Text文件
        BufferedReader br = null;//构造一个BufferedReader类来读取文件
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String s = null;
        try {
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                list.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


}
