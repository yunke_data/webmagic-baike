package yunke.baike.process;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import us.codecraft.webmagic.*;
import us.codecraft.webmagic.downloader.Downloader;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import java.io.IOException;
import java.util.List;

/**
 * Created by admin on 2017/1/9.
 */
public class RuanJian implements PageProcessor{

    private Site site = Site.me().setRetryTimes(3).setRetrySleepTime(3000).setTimeOut(60000)
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");

    int count = 0;
    @Override
    public void process(Page page) {

        if(page.getUrl().regex("p=[0-9]+").match()){
            count++;
            page.addTargetRequests(page.getHtml().xpath("//p[@class='mDetailContentLi2P1 clearfix']/a").links().all());
            page.addTargetRequest(page.getUrl().toString().replaceAll("p="+count,"p="+(count+1)));
        }else{
            Document doc1 = null;
            Document doc2 = null;
            try {
                doc1 = Jsoup.connect(page.getUrl().toString().replaceAll("index.phtml","")+"contact.phtml").userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36").get();
                doc2 = Jsoup.connect(page.getUrl().toString().replaceAll("index.phtml","")+"introduction.phtml").userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36").get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Elements eles = doc1.select("ul[class=linkUs] li");
            page.putField("名称",eles.get(0).text().replaceAll("公司名称：",""));
            page.putField("联系人",eles.get(1).text().replaceAll("联系人",""));
            page.putField("职位",eles.get(2).text().replaceAll("职位",""));
            page.putField("电话",eles.get(3).text().replaceAll("电话",""));
            page.putField("手机",eles.get(4).text().replaceAll("手机",""));
            page.putField("邮箱",eles.get(5).text().replaceAll("电子邮箱",""));
            page.putField("地址",eles.get(6).text().replaceAll("地址",""));
            page.putField("网站",eles.get(7).text().replaceAll("官方网站",""));
            page.putField("介绍",doc2.select("p[class=nbr]").text());
        }
    }

    public Site getSite() {
        return site;
    }


    public static void main(String[] args) {
        Spider.create(new RuanJian())
                //.addUrl("http://www.xuanruanjian.com/service.php?p=1")
                .addUrl("http://www.xuanruanjian.com/service.php?p=1")
                .addPipeline(new JsonFilePipeline("D:/Data/"))
                .addPipeline(new ConsolePipeline())
                .thread(5)
                .run();
    }
}
